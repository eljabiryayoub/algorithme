package helper;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Helper {

    public static void displayArray(int[] input, DisplayType display) {
        String str = Arrays.stream(input).mapToObj(e -> Integer.toString(e)).collect(Collectors.joining(","));
        switch (display) {
            case ERROR:
                System.err.println(str);
                break;
            case INFO:
            default:
                System.out.println(str);
        }
    }

    public static int[] subArray(int[] array, int beg, int end) {
        return Arrays.copyOfRange(array, beg, end + 1);
    }

    public enum DisplayType {
        INFO, ERROR

    }

}
