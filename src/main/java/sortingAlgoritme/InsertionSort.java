package sortingAlgoritme;

public class InsertionSort implements Sort {


//    We start with insertion sort, which is an efficient algorithm for sorting a small
//    number of elements. Insertion sort works the way many people sort a hand of
//    playing cards. We start with an empty left hand and the cards face down on the
//    table. We then remove one card at a time from the table and insert it into the
//    correct position in the left hand. To find the correct position for a card, we compare
//    it with each of the cards already in the hand, from right to left, as illustrated in
//    Figure 2.1. At all times, the cards held in the left hand are sorted, and these cards
//    were originally the top cards of the pile on the table.
    public static int[] sort(int[] input) {
        for (int i = 1; i < input.length; i++) {
            int current = input[i];
            int j = i - 1;
            while (j >= 0 && input[j] > current) {
                input[j + 1] = input[j];
                j--;
            }
            input[j + 1] = current;
        }

        return input;
    }

    public static int[] reverseSort(int[] input) {
        for (int i = input.length - 1; i > 0; i--) {
            int current = input[i];
            int j = i + 1;
            while (j < input.length && input[j] < current) {
                input[j - 1] = input[j];
                j++;
            }
            input[j - 1] = current;
        }

        return input;
    }

    public static int[] reversedSort(int[] input) {
        for (int i = 1; i < input.length; i++) {
            int current = input[i];
            int j = i - 1;
            while (j >= 0 && input[j] < current) {
                input[j + 1] = input[j];
                j--;
            }
            input[j + 1] = current;
        }
        return input;
    }

}
