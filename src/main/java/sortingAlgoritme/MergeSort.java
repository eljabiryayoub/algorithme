package sortingAlgoritme;

public class MergeSort {

public static void mergeSort(int[] array, int p, int r){

    if(p<r){
        int q= (r+p)/2;
        mergeSort(array,p,q);
        mergeSort(array,q+1,r);
        mergeArray(array,p,q,r);
    }
}

    //p<=q<r
    public static void mergeArray(int[] array, int p, int q, int r) {
        int[] leftArray = new int[q - p + 1 +1]; //(+1 for sentinel)
        int[] rightArray = new int[r - q +1]; //(+1 for sentinel)
        for (int i = 0; i < leftArray.length-1; i++) {
            leftArray[i] = array[p + i];
        }
        for (int j = 0; j < rightArray.length-1; j++) {
            rightArray[j] = array[q + j+1];
        }
        leftArray[q - p + 1] = Integer.MAX_VALUE;
        rightArray[r - q] = Integer.MAX_VALUE;

        int i = 0, j = 0;
        for (int k = p; k <= r; k++) {
            if (leftArray[i] <= rightArray[j]) {
                array[k] = leftArray[i];
                ++i;
            } else {
                array[k] = rightArray[j];
                ++j;
            }
        }
    }
}
