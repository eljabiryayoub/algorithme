package sortingAlgoritme;


public class SelectionSort {
//    Consider sorting n numbers stored in array A by first finding the smallest element
//    of A and exchanging it with the element in AŒ1. Then find the second smallest
//    element of A, and exchange it with AŒ2. Continue in this manner for the first n 1
//    elements of A. Write pseudocode for this algorithm, which is known as selection
//    sort. What loop invariant does this algorithm maintain
    public static int[] selectionSort(int[] input) {
        int[] output = new int[input.length];
        for (int i = 0; i < input.length; i++) {
            int indexSmallest  = findSmallestElementIndex(input);
            output[i] = input[indexSmallest];
            input[indexSmallest] = input[i];
            input[i] = Integer.MAX_VALUE;
        }
        return output;
    }

    public static int findSmallestElementIndex(int[] input) {
        int smallestIndex = 0;
        for (int i = 1; i < input.length; i++) {
            if (input[smallestIndex] > input[i]) {
                smallestIndex = i;
            }
        }
        return smallestIndex;
    }
}
