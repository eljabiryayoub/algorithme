package SearchAlgorithme;

public class LinearSearch {
    public static Integer linearSearch(int[] input, int v) {

        for (int i = 0; i < input.length; i++) {
            if(input[i]==v)
                return i;
        }
        return null;
    }
}
