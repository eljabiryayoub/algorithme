package testHelper;

import java.util.Random;

public class Generate {

    public static int[] createData(int N) {
        int[] data = new int[N];
        Random random = new Random();

        for (int i = 0; i < N; i++) {
            data[i]=random.nextInt(100);
        }
        return data;
    }
}
