package sortingAlgoritme;


import helper.Helper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import testHelper.Generate;

import static helper.Helper.displayArray;
import static sortingAlgoritme.InsertionSort.reversedSort;
import static sortingAlgoritme.InsertionSort.sort;

class InsertionSortTest {

    private int[] input;

    @BeforeEach
    void insertValues() {
        this.input = new int[]{2, 5, 3, 10, 0, 1, 9};

//        this.input = Generate.createData(100);
//        displayArray(input);
    }

    @Test
    void sort_Test() {

        int[] output = sort(input);
        displayArray(output, Helper.DisplayType.INFO);
    }

    @Test
    void reversedSort_test() {
        int[] reverseOutput = reversedSort(input);
        displayArray(reverseOutput,Helper.DisplayType.INFO);
    }
}