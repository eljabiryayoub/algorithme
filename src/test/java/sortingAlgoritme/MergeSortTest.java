package sortingAlgoritme;

import helper.Helper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import testHelper.Generate;

import java.time.Duration;
import java.time.Instant;

class MergeSortTest {
    private int[] input;

    @BeforeEach
    void insertValues() {
        this.input = new int[]{2, 4, 5, 1, 3, 6, 19};
    }

    @Test
    void mergeArray() {
        MergeSort.mergeArray(input, 0, 2, 6);
        Helper.displayArray(input, Helper.DisplayType.INFO);

    }

    @Test
    void mergeSort_test() {
        mergeSort(Generate.createData(1));
        mergeSort(Generate.createData(2));
        mergeSort(Generate.createData(3));
    }

    void mergeSort(int[] input) {
        Helper.displayArray(input, Helper.DisplayType.INFO);
        long startTime = System.nanoTime();
        MergeSort.mergeSort(input, 0, input.length - 1);
        long stopTime = System.nanoTime();
        System.out.println(stopTime - startTime);
        Helper.displayArray(input, Helper.DisplayType.ERROR);
    }
}