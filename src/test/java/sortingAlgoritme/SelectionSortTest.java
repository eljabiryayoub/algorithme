package sortingAlgoritme;

import helper.Helper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static helper.Helper.displayArray;
import static sortingAlgoritme.SelectionSort.findSmallestElementIndex;

class SelectionSortTest {

    private int[] input;

    @BeforeEach
    void insertValues() {
        this.input = new int[]{2, 5, 3, 10, 0, 1, 9};
    }

    @Test
    void selectionSort_test() {
        int[] output =SelectionSort.selectionSort(input);
        displayArray(output, Helper.DisplayType.INFO);

    }

    @Test
    void findSmalestElementIndex_test() {
        int index = findSmallestElementIndex(this.input);
        displayArray(input,Helper.DisplayType.INFO);
        System.out.println(input[index]);
    }
}