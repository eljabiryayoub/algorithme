package helper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class HelperTest {
    private int[] input;

    @BeforeEach
    void insertValues() {
        this.input = new int[]{2, 5, 3, 10, 0, 1, 9};
    }

    @Test
    void subArray() {
        Helper.displayArray(Helper.subArray(input, 2, input.length - 1), Helper.DisplayType.INFO);
    }
}